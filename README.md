# ********** This repository has been moved to [[https://forge.superkamiguru.org/MSG/msg-cli]] ************


# MSG (Mac Subsystem for Guix)
## Description
MSG is an attempt to create a native-like experience for GUIX on MacOS, using methods inspired by others like Podman/Docker/Lima.


# !!!!Currently only compatible with M1/M2 Macs
## README (WIP)


**REQUIREMENTS**
- xquartz (if you want to run graphical applications)
    - You also must go to Xquartz->Settings->Security->Enable 'Allow connections from network clients'
- Add the following to your shellrc file (~/.bashrc, ~/.zshrc, etc)
    - ```
         alias guix="ssh -i $HOME/.guix/ssh-cert/msg_rsa admin@msg.local 'guix'"
         alias guix-shell="ssh -i $HOME/.guix/ssh-cert/msg_rsa admin@msg.local"
         alias guix-env='f(){ ssh -i $HOME/.guix/ssh-cert/msg_rsa admin@msg.local  /home/admin/.guix-profile /bin/$@;  unset -f f; }; f'
         alias guix-app='f(){ ssh -X -i $HOME/.guix/ssh-cert/msg_rsa admin@msg.local  /home/admin/.guix-profile/bin/$@;  unset -f f; }; f'
      ```

**INSTRUCTIONS**
- Drag the Guix.app file into your Applications directory and run!

**Available Commands (after adding bashrc/zshrc options)**

* guix : A straight passthrough to the guix application
* guix-shell: Connect to the vm over ssh
* guix-env : Can be used to pass cli commands interactively
* guix-app : Used to passthrough X11 applications from the vm to macos using xquartz

If you would like to discuss or ask questions, join us on our Discord!
https://discord.gg/WSceSUhs6t
